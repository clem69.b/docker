## [2.1.1](https://gitlab.com/to-be-continuous/docker/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([5596daf](https://gitlab.com/to-be-continuous/docker/commit/5596daf34cc9c64d73405ef490fe75c7ae50a177))

## [2.0.0](https://gitlab.com/to-be-continuous/docker/compare/1.2.3...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([f74d9ef](https://gitlab.com/to-be-continuous/docker/commit/f74d9ef2de5b4dc204b519e14c47862ea2b73b33))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.3](https://gitlab.com/to-be-continuous/docker/compare/1.2.2...1.2.3) (2021-07-21)

### Bug Fixes

* variable name ([1aed76f](https://gitlab.com/to-be-continuous/docker/commit/1aed76f287ff27e35233941e35cbac1e8ea9d2ff))

## [1.2.2](https://gitlab.com/to-be-continuous/docker/compare/1.2.1...1.2.2) (2021-07-05)

### Bug Fixes

* update skopeo credentials ([559961f](https://gitlab.com/to-be-continuous/docker/commit/559961f9f3c30e241a049ee4ea94e9050be49592))

## [1.2.1](https://gitlab.com/to-be-continuous/docker/compare/1.2.0...1.2.1) (2021-06-25)

### Bug Fixes

* permission on reports directory ([2d2f360](https://gitlab.com/to-be-continuous/docker/commit/2d2f360420b13bbdb71c11910ac8214b74a15901))

## [1.2.0](https://gitlab.com/to-be-continuous/docker/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([405fdcc](https://gitlab.com/to-be-continuous/docker/commit/405fdcc65ef1e95cb5997c610266c1422a06802e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/docker/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fc9a1ad](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/fc9a1adc498209ea9fa7c6eb64831f6e1abe857f))

## 1.0.0 (2021-05-06)

### Features

* initial release ([0b00fba](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/0b00fba9a515d4ea10c35cf38abc01c217f0979a))
